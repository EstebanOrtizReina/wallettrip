import express from 'express';

import set_place from'./set_place.js';

import get_place from'./get_place.js'


const app=express();
const puerto=3000;

//Ruta-Punto de acceso al aplicativo. Interfaz de la API.

app.get('/saludo', (peticion, respuesta)=>{

    respuesta.send("Hola al mundo del Backend!!");
}); 

app.get('/set_place', (peticion, respuesta)=>{

    set_place();
    respuesta.send("Algo Paso!!");

});

app.get("/get_place", (peticion, respuesta)=>{

    get_place();
    respuesta.send();
    
});

// Inicializar el servidor

app.listen(puerto, ()=>{console.log("Esta funcionando el servidor.")});


